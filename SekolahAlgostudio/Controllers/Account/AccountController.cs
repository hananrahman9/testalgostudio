﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SekolahAlgostudio.Data;
using SekolahAlgostudio.Models;
using SekolahAlgostudio.Models.Account;

namespace SekolahAlgostudio.Controllers.Account
{
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<UserViewModel> _userManager;

        public AccountController(ApplicationDbContext dbContext,
            UserManager<UserViewModel> userManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            var listRole = (from role in _dbContext.Roles
                            select new
                            {
                                RoleID = role.Id,
                                Role = role.Name
                            }).ToList();

            ViewBag.ListOfRole = listRole;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel input, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new UserViewModel { UserName = input.Email, Email = input.Email };
                var result = await _userManager.CreateAsync(user, input.Password);

                if (result.Succeeded)
                {
                    var resultRole = await _userManager.AddToRoleAsync(user, input.Role);

                    return RedirectUrl(returnUrl);

                }

                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(input);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectUrl(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

    }
}