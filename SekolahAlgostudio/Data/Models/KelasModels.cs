﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SekolahAlgostudio.Data.Models
{
    [Table("Kelas")]
    public class KelasModels
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string tingkat { get; set; }

        [Required]
        [StringLength(10)]
        public string jurusan { get; set; }

        [Required]
        [StringLength(50)]
        public string waliKelas { get; set; }

        public ICollection<SiswaModels> SiswaModels { get; set; }
    }
}
