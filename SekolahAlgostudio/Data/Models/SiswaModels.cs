﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SekolahAlgostudio.Data.Models
{
    [Table("Siswa")]
    public class SiswaModels
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string NIM { get; set; }

        [Required]
        [StringLength(50)]
        public string nama { get; set; }

        [ForeignKey("MS_Kelas")]
        public int kelasID { get; set; }
        public virtual KelasModels KelasModels { get; set; }
    }
}
