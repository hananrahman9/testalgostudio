﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SekolahAlgostudio.Data.Models;
using SekolahAlgostudio.Models.Account;

namespace SekolahAlgostudio.Data
{
    public class ApplicationDbContext : IdentityDbContext<UserViewModel>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public virtual DbSet<SiswaModels> SiswaModels { get; set; }
        public virtual DbSet<KelasModels> KelasModels { get; set; }

    }
}
